#include <iostream>
#include <string.h>
#include <cmath>

using namespace std;

string split(string str,char sep,int n)
{
    string result = "";
    int count = 0;
    for(int i=0;i < str.size();i++)
    {
        if(str[i] != sep)
        {
            result += str[i];
        }
        else
        {
            count++;
            if(count == n) return result;
            result="";
        }
    }

    return result;
}

int main(int argc,char** argv)
{
    if(argc == 4)
    {
        cout<< split(argv[1],argv[2][0],stoi(argv[3],NULL,10)) << endl;
    }
    else if(argc == 1)//Program without arguments
    {
        printf("USAGE:\n\tsp <text> <separator> <part>\n\nEXAMPLE:\n\tsp \"alex;john;alisa\" ; 3\nOUTPUT:\n\t$> alisa\n");
    }
    else
    {
        printf("Arguments must be 3\n");
    }

    return 0;
}
