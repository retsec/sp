# sp
Split command

```shell
$> ./sp
USAGE:
        sp <text> <separator> <part>
        
EXAMPLE:
        sp "alex;john;alisa" ; 3
OUTPUT:
        $> alisa
```